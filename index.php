<?php
define('DB_HOST', 'localhost');
define('DB_USER', 'vbochkova');
define('DB_PASS', 'neto1837');
define('DB_NAME', 'global');

$isbn = (!empty($_POST['isbn'])) ? $_POST['isbn'] : '';
$name = (!empty($_POST['name'])) ? $_POST['name'] : '';
$author = (!empty($_POST['author'])) ? $_POST['author'] : '';

$connect = 'mysql:host=' . DB_HOST . '; dbname=' . DB_NAME . '; charset=utf8';
$db = new PDO($connect,DB_USER,DB_PASS);

$sql = "SELECT name, author, year, isbn, genre
          FROM books
          WHERE isbn LIKE ? AND name LIKE ? AND author LIKE ?";
$res = $db->prepare($sql);
$res->execute(["%$isbn%", "%$name%","%$author%"]);
$result = $res->fetchAll(PDO::FETCH_ASSOC);
?>

<h1>Библиотека успешного человека</h1>

<form action="" method="POST" enctype="multipart/form-data">
    <input type="text" name="isbn" placeholder="ISBN" value="<?php htmlspecialchars($isbn)?>" >
    <input type="text" name="name" placeholder="Название книги" value="<?php htmlspecialchars($name)?>" >
    <input type="text" name="author" placeholder="Автор книги" value="<?php htmlspecialchars($author)?>" >
    <input type="submit" value="Поиск"/>
</form>

<table border="1" cellpadding="8">
    <tr>
        <th>Название</th>
        <th>Автор</th>
        <th>Год выпуска</th>
        <th>ISBN</th>
        <th>Жанр</th>
    </tr>
    <?php foreach ($result as $data){?>
    <tr>
        <td><?php echo htmlspecialchars($data['name']); ?></td>
        <td><?php echo htmlspecialchars($data['author']); ?></td>
        <td><?php echo htmlspecialchars($data['year']); ?></td>
        <td><?php echo htmlspecialchars($data['isbn']); ?></td>
        <td><?php echo htmlspecialchars($data['genre']); ?></td>
    </tr>
    <?php }; ?>
</table>
